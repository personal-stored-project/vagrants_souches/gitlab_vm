#! /bin/bash

# Setup Firewall
firewall-cmd --permanent --add-service=http
firewall-cmd --reload

# Install Dependencies
curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.rpm.sh | bash
EXTERNAL_URL="http://192.168.50.110" dnf install -y gitlab-ce
